---
1.) It's a combination of prediction and optimization problem.

Online version action-state space:
State:
U_it- hosts turned on @ time t.
X_jit- (alive) VM's currently assigned to machine t
Action space:
Move around X_jit- no cost or assumed small constant cost 
(in reality depends of ram of host, size and network)
Turn on new hosts (but it's up only after 3 time periods)

2) The integer program

min \sum U_it
subject to
forall i\in U, t in 1..T : c_i*U_it>= sum_j W_ijt*x_ijt #capacity
forall \j in J_t,t: sum_i x_ijt==1 #demand
forall i\in U, t in 1..T: u_it>=u_i(t-1)+switch_i(t-3)

(optional 'machine rearrangement cost'
move_ijt>=abs(x_ijt-x_ij(t-1))
and add 
sum(const*move_ijt) to the cost function
also add variable consumption
)


--- currently modelled as 1D binning(RAM), adding more dimensions is trivial,
just repeat the capacity constraint, but didn't do it, plus 
every constraint, other than RAM and HD is much softer (w/ unclear parameters).

Scalability- we can easily solve w/ off the shelf solver for 100 periods
 Loose coupling between knapsacks at different time periods, can exploit this to 
solve for arbitrary period.

3) Adding the service level constraint:
VMware allocates overcapacity on purpose, so we also have to use 'pessimistic' estimates to run
optimizer on.
-- Can move around VM's quickly even in 'penalty mode'.
-- Point forecasts are useless, need something w/ quantiles
3sd service level on the level of the center, so with

-- forecast::tbats w/ seasonalities @ 1,6,24 hours gives for 3-step ahead
forecast around 8% total 'absolute' error (sum(abs(x-y))/sum(y)) for the RAM, around 24% rmspe
and allows for quantile estimation.

--- Find appropriate 'pessimistic' evaluation. 
this is a function
Peval:state*forecast->state

* foreach vm, take Peval(vm,t+1)=max(W(vm,t') for t in (t-12,t+1))- guarantee never better than actual situation
* use central limit theorem need 3sd worst in the sense of Datacenter, 
if calculations are correct,
\[Mu] + 0.0977217 \[Sigma]  inflating each individual forecast w/ 0.1*sd is good enough(add the max
of the actual just to be certain).







