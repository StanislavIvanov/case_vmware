import ortools.linear_solver.pywraplp as pl
import math
import pandas as pd
import random
import string


# m=pl.Solver("vmware",pl.Solver.CBC_MIXED_INTEGER_PROGRAMMING)
m=pl.Solver("vmware",pl.Solver.BOP_INTEGER_PROGRAMMING)

time_horizon=100
time_index=range(time_horizon)
num_hosts=16
host_index=range(num_hosts)
import numpy as np

def generate_task(min_start_time=0,mean_duration=5 ,max_end_time=time_horizon,
                  distribution_demand=lambda x:np.random.uniform(0,5,size=x)):
    task_begin=np.random.uniform(min_start_time,
                                 math.floor(max_end_time-(max_end_time-min_start_time)/2))
    task_begin=int(math.floor(task_begin))
    task_end=int(np.random.uniform(
        task_begin+1,task_begin+1+mean_duration*2
    ))

    task_end=int(math.floor(max(task_end,max_end_time)))
    task_len=int(task_end-task_begin+1)
    distribution_demand=distribution_demand(task_len)
    time_ind=range(task_begin,task_end+1)
    # print len(time_ind)
    # print len(distribution_demand)
    key=''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))

    return pd.DataFrame({'key':key,'time':time_ind,'demand':distribution_demand})
    # return {'start':task_begin,'end':task_end,'demand':list(distribution_demand)}


# print generate_task(0,10,3)
#

import numpy as np
import pandas as pd
import itertools

def cartesian(df1, df2):
    rows = itertools.product(df1.iterrows(), df2.iterrows())

    df = pd.DataFrame(left.append(right) for (_, left), (_, right) in rows)
    return df.reset_index(drop=True)

# m=mp.Model("vmware")
machines=pd.DataFrame({'host':host_index})
times=pd.DataFrame({'time':time_index})
pd_uit=cartesian(machines,times)

# uit=[m.IntVar(0,1,str(i)+'safdsad') for i in pd_uit.index.tolist()]
uit=[m.BoolVar(str(i)+'safdsad') for i in pd_uit.index.tolist()]

pd_uit['uit']=uit
# swtich-on constraints
# swit=m.BoolVar(pd_uit.index.tolist(),0,1,
#                       pd_uit.apply(lambda x:'sw'+str(x['host'])+"_"+str(x['time']),axis=1)
#                        )

swit=[m.BoolVar('') for _ in pd_uit.index.tolist()]
pd_uit['swit']=swit

unique_times=pd_uit['time'].drop_duplicates().tolist()
times_after_first=unique_times[2:]
grp0=pd_uit.groupby(['host'])
if True:
    for _, grp in grp0:
        kk=grp.sort_values('time')
        for l in range(3,kk.shape[0]):
            m.Add(
                kk.iloc[l]['uit']==kk.iloc[l-1]['uit']+kk.iloc[l-3]['swit']
        )


x_obj=m.Sum(pd_uit['uit'].tolist())
m.Minimize(x_obj)
print pd_uit

num_tasks=50
generated_tasks=[generate_task() for _ in range(num_tasks)]
generated_tasks=pd.concat(generated_tasks)
# print generated_tasks

moo=pd_uit.merge(generated_tasks,how='inner',left_on='time',right_on="time")
# moo['xijt']=m.binary_var_list(list(moo.index),0,1,moo.apply(lambda x:str("u")+"_"+str(x['host'])+"_"+str(x['time'])+"_"+str(x['key']),axis=1))
# moo['xijt']=[m.IntVar(0,1,'') for i in list(moo.index)]
moo['xijt']=[m.BoolVar('') for i in list(moo.index)]

print moo

# add capacity constraints
host_capacity=15
grpby1=moo.groupby(['host',"time"])
for nm,grp in grpby1:
    print nm
    print grp
    expr=grp['uit'].tolist()[0]
    print expr
    m.Add(expr*host_capacity>=m.Sum(l*k for k,l in zip(grp['xijt'].tolist(),grp['demand'].tolist())))

# add assignment constraints
grpby2=moo.groupby(['key',"time"])
for _,grp in grpby2:
    m.Add(m.Sum(grp['xijt'].tolist())==1)



# add switching costst

x_obj=m.Sum(pd_uit['uit'].tolist())
m.Minimize(x_obj)
if True:
    # moo['swijt']=m.binary_var_list(list(moo.index),0,1,moo.apply(lambda x:str("switch")+"_"+str(x['host'])+"_"+
    #                                                                       str(x['time'])+"_"+str(x['key']),axis=1))
    moo['swijt']=[m.BoolVar('') for i in list(moo.index)]

    grpby_switch=moo.groupby(['host','key'])
    for _,grp in grpby_switch:
        ll=grp.sort_values("time")
        switch_constraint=[m.Add(
            i<=j+k
        ) for i,j,k in zip(grp['swijt'][1:].tolist(),grp['xijt'][1:].tolist(),grp['xijt'][:-1].tolist())]

        ll1=grp.sort_values("time")
        switch_constraint=[m.Add(
            i>=j+k
        ) for i,j,k in zip(grp['swijt'][1:].tolist(),grp['xijt'][1:].tolist(),grp['xijt'][:-1].tolist())]

    x_obj=x_obj+m.Sum(moo['swijt'])*0.02
    m.Minimize(x_obj)





m.set_time_limit(800000)
m.output_level=2
print pd_uit

kk=m.Solve()
print m.Objective().Value()


if False:
    tasks_index=range(len(generated_tasks))

    xjit={i:{t:{j:m.binary_var('x{}_{}_{}'.format(j,i,t)) for j in tasks_index if generated_tasks[j]['start']<=t and
                generated_tasks[j]['end']>=t
    }  for t in time_index}  for i in host_index}
    print tasks_index
    import pandas as pd
    xjit_df=pd.DataFrame.from_dict(xjit)
    print xjit_df

    Wjit={i:{t:{j:generated_tasks[j]['demand'][
        int(t-generated_tasks[j]['start'])
    ] for j in tasks_index if generated_tasks[j]['start']<=t and
                generated_tasks[j]['end']>=t
                }  for t in time_index}  for i in host_index}
    # assignment constraint:











