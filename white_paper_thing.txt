---
  title: "White Paper Report"
author: Stefan Petrov, Stanislav Ivanov, Alexandar Aldev 
output: html_document 
---
  
  
  # VM Ware- optimizing power load
  There is a data center w/ 16 host machines, and some VM's, allocated on those.
Rearranging VM's between hosts is easy, but hosts being powered is costly.
Sampling is 10 minutes.
## Online optimization/prediction
It's a combination of prediction and optimization problem.

### Online version action-state space:
State:
U_it- hosts turned on @ time t.
X_jit- (alive) VM's currently assigned to machine t
Action space:
  Move around X_jit- no cost or assumed small constant cost 
(in reality depends of ram of host, size and network)
Turn on new hosts (but it's up only after 3 time periods)
                   
                   ### Offline look-ahead integer program implementation
                   min \sum U_it
                   subject to
                   forall i\in U, t in 1..T : c_i*U_it>= sum_j W_ijt*x_ijt #capacity
                   forall \j in J_t,t: sum_i x_ijt==1 # also forall resources:storage, RAM, CPU, network
                   forall i\in U, t in 1..T: u_it>=u_i(t-1)+switch_i(t-3)
                   
                   (optional 'machine rearrangement cost'
                   move_ijt>=abs(x_ijt-x_ij(t-1))
                   and add 
                   sum(const*move_ijt) to the cost function
                   also add variable consumption
                   )
                   
                   Modelled for RAM only at the moment- hard to put 'soft constraints' on stuff otherwise.
                   --- currently modelled as 1D binning(RAM), adding more dimensions is trivial,
                   just repeat the capacity constraint, but didn't do it, plus 
                   every constraint, other than RAM and HD is much softer (w/ unclear parameters).
                   
                   Scalability- we can easily solve w/ off the shelf solver for 100 periods
                   Loose coupling between knapsacks at different time periods, can exploit this to 
                   solve for arbitrary period.
                   
                   ## Service Level Constraint and forecasting
                   
                   ### Demand must be satisfied 99.9% of the time
                   If using forecast, need to forecast 'pessimistically' to simulate online 
                   server manager decision making- under-prediction is potentially >> over-prediction.
                   
                   ### Preferable to return models that return distribution
                   Pessimistic forecast generation algorithm:
                     from current state, use forecasts to generate 'pessimistic' vm configuration.
                   This needs to be about the 99.9% quantile of prediction on the level of the whole datacenter.
                   So use current actual case ( state ) to generate current 'bad' case, which
                   we append to the optimizer data.
                   
                   for t in 1:T:
                     pessimistic_config=find_bad_config(state(t))
                   optimize(pessimistic_config)
                   Ideas: 
                     pessimistic_config(t)=c*state(t)# used c=1.3 and c=1.5 for experiments
                   2) Use central limit theorem +forecast to generate bad config:
  Mu] + 0.29425 Sigma
so useing mu+0.3*sigma foreach forecast on the vm-level

also should give a prediciton that's *bad enough*.
Have to do this properly for a proper business case.




# Results - 
##Optimization results
1.) VMware cost- (fixed cost comparison only)
1.3x  1.5x actual
horizon 20- 109 vs 123 vs 207
horizon 30- 189 vs 217 vs 310
horizon 40- 349 vs 373 vs 414


This is realistic, since it's fixed-cost only+ only ram modelled.

##Forecasting model results
Tested R's forecast on tbats with the following parameters:

tbats(x,seasonal=c(6,6*6, 24*6))#foreach vm 

forecasting results managed to run only for part of the vm's
Total error 3 ticks ahead:
  0.08394983
collected[,sqrt(sum(prediction-true_value)^2/.N)/(mean(true_value)))]
Mean squared pct error:
  0.2248227
collected[,sqrt(sum(prediction-true_value)^2/.N)/(max(true_value)-min(true_value))]
Normalized Squared error:
  0.0432197





entity_id         V1
1: 4216c08e-bdae-a35f-06bf-0f437d47cf84 0.04576840
2: 421cf118-1e2a-4ae0-cbe9-c79526418fc7 0.27020916
3: 421cd40b-35de-a7c9-2a71-3964b34f84e0 0.19889644
4: 42162788-d12c-6f5a-e8b4-b6c6d00818b4 0.03414912
5: 4216afd4-27f7-40ed-0eed-aed7a0777d4c 0.09825410
6: 4216a3de-cc76-d8ef-38ef-f70fa85ae61e 0.07272515
7: 4219bc8b-8e07-dc57-6fd0-e4ac733b6cbe 0.09923004
8: 42161a1e-108a-ed9a-9a91-af3afcc35a0d 0.28488178
9: 4224c267-67aa-9393-a978-a6c5db8432e8 0.12560703
10: 4224c878-a6dd-5d45-321c-0e97e9854a8e 0.03482750
11: 42163ea4-26e7-f393-3347-717532997a45 0.08357135
12: 4216674c-de33-a121-3a9c-d718728c08a5 0.09926260
13: 4216250d-7d74-aba3-3999-8408c97b5779 0.14312347
14: 42118ba8-847c-0b83-be3b-927f7a46c2f0 0.11842502
15: 42160b25-a520-8cc6-cc43-1da7fc420d88 0.06681501
16: 4216f829-c6af-7044-4419-bdc129059b0a 0.14836718
17: 422490ee-0672-d78b-fb46-a6b33d7d1cea 0.08612554
18: 4216e8dc-a1ff-eb07-54e2-a6621b163264 0.42682241
19: 4224df24-3e5f-9aaf-2d38-ea713a7c217f 0.06596282
20: 42118288-df9c-e366-2999-1c276fabf18c 0.03579260
21: 421608be-daba-fec9-c17a-c35ce457cbd1 0.08607061
22: 4211d7a3-3cb9-88ca-c2c2-d62a0144b297 0.00913523
23: 4216a6bc-70f3-ab56-ef85-db3f56a119f5 0.22549574
24: 4211880c-9402-6eb5-488a-79013bffd2c5 0.11836827
25: 4216dec6-ee2f-05f0-4611-9c225504f7d2 0.11191587
26: 4216dd02-beef-a27f-a470-e453b2828754 0.07981613
27: 4224704b-aca1-2d13-c774-c6bafa5bb451 0.06375596
28: 42168b75-625e-b320-368e-7966f1bad8b4 0.21859180
29: 42249776-af66-f188-8cbe-d1d4ae58c714 0.02358901
30: 42164901-699e-3c50-e00b-ab643da2848f 0.15380119
31: 4224e3e5-9f99-7d09-81b2-04ec04363d58 0.02521980
32: 42118a13-dc1d-0c3e-6547-06b31f980028 0.05981342
33: 42165335-f0ee-6f1e-84d5-05db9f75322b 0.28199814
34: 42111a3b-46e7-88ed-c127-958cdb817251 0.06349230
35: 4216e490-78cf-cc77-9d07-26b201b29e09 0.05233007
36: 4216b986-323f-3cc2-6c29-0c1b09cf4a6a 0.08619610
37: 421641db-5a4b-2452-9e98-7d2b38abbe55 0.06841455
38: 42165d2d-bdb3-f868-25a3-a9cd1ad13c02 0.05657680
39: 423faa51-ff4a-3d1e-c125-75549b30821b 0.16635442
40: 42242dec-370b-598f-9c68-de5e69cef404 0.08464415
41: 4216d3bf-75c0-ea37-cd45-733a9dffff18 0.10897905
42: 4216dbee-b029-4c11-8789-c95923aa9ee9 0.04703750
43: 421632ba-1d82-cf4e-f609-c72c686c2145 0.25000343
44: 4216242d-7b2f-8eda-f48f-69e159f2b8dc 0.49699344
45: 4216ebfb-b618-f0e2-483b-6f6c838b93b4 0.05254836
46: 42112d10-ee02-f839-b10b-a9550ee8c07b 0.06164548
47: 421c56df-743c-3e11-f245-3de8ad6e3018 0.15793932
48: 4216949a-a5c1-d38b-74be-63fb41d815ec 0.28492967
49: 4211a9d0-fc47-a301-b806-bda2dc45f048 0.12563619
50: 42161dd6-86e9-80a5-ede2-941a32704419 0.07332830
51: 4216261b-6d05-5629-463d-cc32de0d6c41 0.07273802
52: 4216287a-d851-a124-6121-33f93438e2b6 0.04445887
53: 421689b2-d5d3-4a95-6121-0b6ee3c6ea88 0.09779519
54: 421623cb-f179-a723-d3d3-b455b730fbce 0.05853695
55: 4216d1d2-7b3b-50b7-e396-bbf2ae21e239 0.09136602
56: 4216e22b-54e9-2dff-a4eb-109df6e25be9 0.06558828
57: 4216737c-62cb-2ef5-7726-2a9a618217be 0.08621812
58: 4216fd79-cdea-ea60-30f9-04c0620c29e6 0.05287713
59: 421614ad-d36e-6a8f-3a0a-53815278d2b1 0.06402681
60: 421613b0-3fd4-ddc3-f176-625ad5edea2b 0.05796342
61: 42243e13-e0d9-f9b0-81d6-d3217c20f3f0 0.02713961
62: 421601a8-6eb5-011b-99a9-7f4c4e161fe3 0.06355865
63: 42164ac1-26b0-e432-dc1d-339126898935 0.26751816
64: 4216a257-c2ba-5971-c04f-382b7c8b2588 0.05902410
65: 42161969-44d8-77b0-cfdf-9a2b4d39db35 0.07376797
66: 4216f9a3-c7ed-4238-7a6c-51b7b9e83bce 0.12809223
67: 421686f5-4a7b-1e15-ae98-44aa3f3555e7 0.03929820
68: 4216444e-a0c8-ab12-0c7a-eb5bb4f817b2 0.04904600
69: 42111751-2c77-6eb0-86c4-f2b2e95340d8 0.03064597
70: 4216fb0f-0082-30ca-680a-c7c5bd0a3902 0.17226272
71: 421648c2-d64b-24c5-af7b-6d1edccaa2d7 0.09183768
72: 42168dc3-c798-2bf2-50da-31c7c2f283ec 0.21439753
73: 4216d0c9-668d-2014-079e-4498d555d2e1 0.08247401
74: 4216f5f2-4203-41cb-310b-f92093b363db 0.06448743
75: 42168d05-8cd4-29f2-89f7-fc526bc9f500 0.11547278
76: 4216da93-b119-0309-a16e-b147cf18574d 0.06357073
77: 42163316-218b-9fec-2bea-3de89739b7f2 0.11019383
78: 42161694-510a-275a-8261-d26a01c42b19 0.36572605
79: 42164b6e-ea85-b460-aacf-5121888207cf 0.07110438
80: 42164200-1166-928e-947d-6681739644c7 0.06319641
81: 42242605-28d2-4336-19c9-4d5817f71e56 0.14036148
82: 42161bcd-a7c0-31e7-0aa7-c55401afa694 0.06362346
83: 4216f313-66a5-db25-fcfe-a4f83dbce49b 0.06753651
84: 4216a976-81fe-f6a8-fc50-e6d3435fb2fc 0.22860637
85: 4216707e-3c57-4501-f719-d10a5e910854 0.05257253
86: 4216e481-faf5-14f0-0a26-5b17abcda268 0.10801455
87: 4216717b-eae6-4f08-4169-4ecd8a82261a 0.05843447
88: 4216f7f7-1aa8-e2bb-be01-b217fcad6d07 0.05384236
entity_id         V1








### Pessimistic forecast for the purposes of optimization
Loss function highly non-linear/non-symmetric.
















